<?php

use App\Model\Jadwal;
use Illuminate\Database\Seeder;

class JadwalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jadwal::create([
            'kelas_id' => '01',
            'mapel_id' => '02',
            'guru_id' => '1',
            'hari' => 'senin',
            'jam_pelajaran' => '08.00 - 10.00 WIB'
            ]);
    }
}
