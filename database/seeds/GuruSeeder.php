<?php

use App\Model\Guru;
use Illuminate\Database\Seeder;

class GuruSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Guru::create([
            'user_id' => '1',
            'nip' => '2',
            'nama' => 'Arazka Firdaus Anavyanto',
            'tempat_lahir' => 'Gunung Kidul',
            'tgl_lahir' => '20',
            'gender' => 'laki-laki',
            'phone_number' => '08123456789',
            'email' => 'arazxa12@gmail.com',
            'alamat' => 'muntilan',
            'pendidikan' => 'mahasiswa'
            ]);
    }
}
