<?php

use App\Model\Presensi;
use Illuminate\Database\Seeder;

class PresensiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Presensi::create([
            'siswa_id' => '1',
            'status' => 'hadir'
            ]);
    }
}
