<?php

use App\Model\Siswa;
use Illuminate\Database\Seeder;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Siswa::create([
            'nis' => '2005040040',
            'nama' => 'Arazka Firdaus Anavyanto',
            'gender' => 'laki-laki',
            'tempat_lahir' => 'Gunung Kidul',
            'tgl_lahir' => '20',
            'email' => 'arazxa22@gmail.com',
            'nama_ortu' => 'Joko Widodo',
            'alamat' => 'muntilan',
            'phone_number' => '086726427224',
            'kelas_id' => '001'
            ]);
    }
}
